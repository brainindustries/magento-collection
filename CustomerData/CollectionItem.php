<?php

namespace Shirtplatform\Collection\CustomerData;

use Magento\Catalog\Model\Product\Configuration\Item\ItemResolverInterface;
use Shirtplatform\Core\Helper\Image as CoreImageHelper;
use Magento\Framework\View\ConfigInterface;
use Magento\Catalog\Helper\Image;
use Magento\Msrp\Helper\Data;
use Magento\Framework\UrlInterface;
use Magento\Catalog\Helper\Product\ConfigurationPool;
use Magento\Checkout\Helper\Data as CheckoutHelper;
use Magento\Framework\Escaper;
use Magento\Framework\Serialize\SerializerInterface;

class CollectionItem extends \Magento\Checkout\CustomerData\DefaultItem
{

    /**
     *
     * @var CoreImageHelper
     */
    private $_coreImageHelper;

    /**
     * @var SerializerInterface
     */
    private $_serializer;

    /**
     * View config model
     *
     * @var ConfigInterface
     */
    private $_viewConfig;

    /**
     * 
     * @param CoreImageHelper $coreImageHelper
     * @param ConfigInterface $viewConfig
     * @param Image $imageHelper
     * @param Data $msrpHelper
     * @param UrlInterface $urlBuilder
     * @param ConfigurationPool $configurationPool
     * @param CheckoutHelper $checkoutHelper
     * @param SerializerInterface $serializer
     * @param Escaper $escaper
     * @param ItemResolverInterface $itemResolver
     */
    public function __construct(
        CoreImageHelper $coreImageHelper,
        ConfigInterface $viewConfig,
        Image $imageHelper,
        Data $msrpHelper,
        UrlInterface $urlBuilder,
        ConfigurationPool $configurationPool,
        CheckoutHelper $checkoutHelper,
        SerializerInterface $serializer,
        Escaper $escaper = null,
        ItemResolverInterface $itemResolver = null
    ) {
        parent::__construct($imageHelper, $msrpHelper, $urlBuilder, $configurationPool, $checkoutHelper, $escaper, $itemResolver);
        $this->_coreImageHelper = $coreImageHelper;
        $this->_serializer = $serializer;
        $this->_viewConfig = $viewConfig;
    }

    /**
     * Get quote item data for minicart. Add collection_product product type and 
     * designed images.
     * 
     * @access protected
     * @return array
     */
    protected function doGetItemData()
    {
        $itemData = parent::doGetItemData();
        $itemData['product_type'] = 'collection_product';
        $itemData['designed_images'] = [];
        $itemData['mouseover_images'] = [];

        $designedImagesVars = $this->_viewConfig->getViewConfig()->getVarValue('Shirtplatform_Collection', 'designed_images');
        $mouseoverImagesVars = $this->_viewConfig->getViewConfig()->getVarValue('Shirtplatform_Collection', 'mouseover_images');

        if (isset($designedImagesVars['width']) && isset($designedImagesVars['height'])) {
            $images = $this->_coreImageHelper->getCollectionImages($this->item, ['width' => $designedImagesVars['width'], 'height' => $designedImagesVars['height']]);
        } else {
            $images = $this->_coreImageHelper->getCollectionImages($this->item, ['width' => 70, 'height' => 70]);
        }

        if (isset($mouseoverImagesVars['width']) && isset($mouseoverImagesVars['height'])) {
            $mouseoverImages = $this->_coreImageHelper->getCollectionImages($this->item, ['width' => $mouseoverImagesVars['width'], 'height' => $mouseoverImagesVars['height']]);
        } else {
            $mouseoverImages = $this->_coreImageHelper->getCollectionImages($this->item, ['width' => 150, 'height' => 150]);
        }        

        //the result subarray should be indexed from 0 due to knockout template
        foreach ($images as $_image) {
            $itemData['designed_images'][] = $_image;
        }
        foreach ($mouseoverImages as $_image) {
            $itemData['mouseover_images'][] = $_image;
        }

        // if (is_array($itemData['options'])){
        //     $itemData['options'] = $this->_serializer->serialize($itemData['options']);
        // }

        return $itemData;
    }
}
