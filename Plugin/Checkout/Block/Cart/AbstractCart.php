<?php

namespace Shirtplatform\Collection\Plugin\Checkout\Block\Cart;

use Shirtplatform\Core\Model\Config\Source\ProductType;

class AbstractCart {

    /**
     * Get item row html. Use "collection" item renderer for shirtplatform collection products
     * 
     * @access public
     * @param \Magento\Checkout\Block\Cart\AbstractClass $subject
     * @param Closure $proceed
     * @param \Magento\Quote\Model\Quote\Item $item
     * @return string
     */
    public function aroundGetItemHtml($subject,
                                      $proceed,
                                      $item)
    {
        if ($item->getShirtplatformProductType() == ProductType::COLLECTION_PRODUCT) {
            $renderer = $subject->getItemRenderer('collection_product')->setItem($item);
            return $renderer->toHtml();
        }
        return $proceed($item);
    }

}
