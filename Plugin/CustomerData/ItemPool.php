<?php

namespace Shirtplatform\Collection\Plugin\CustomerData;

use Magento\ConfigurableProduct\Model\Product\Type\Configurable as ConfigurableProduct;
use Shirtplatform\Core\Model\Config\Source\ProductType;

class ItemPool {    
    /**     
     * @var \Shirtplatform\Collection\CustomerData\CollectionItem
     */
    private $_collectionItem;
    
    /**
     *     
     * @param \Shirtplatform\Collection\CustomerData\CollectionItem $collectionItem
     */
    public function __construct(\Shirtplatform\Collection\CustomerData\CollectionItem $collectionItem) {
        $this->_collectionItem = $collectionItem;
    }
    
    /**
     * Get item data for product. All designed images are returned for collection product.
     * 
     * @access public
     * @param \Magento\Checkout\CustomerData\ItemPoolInterface $subject
     * @param \Closure $proceed
     * @param \Magento\Quote\Model\Quote\Item $item
     * @return array
     */
    public function aroundGetItemData($subject, $proceed, $item) {                
        if ($item->getProductType() == ConfigurableProduct::TYPE_CODE) {
//            $product = $item->getProduct();
            
            if ($item->getShirtplatformProductType() == ProductType::COLLECTION_PRODUCT) {
                return $this->_collectionItem->getItemData($item);
            }            
        }
        
        return $proceed($item);
    }
}