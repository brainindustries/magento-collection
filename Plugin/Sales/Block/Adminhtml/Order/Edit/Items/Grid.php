<?php

namespace Shirtplatform\Collection\Plugin\Sales\Block\Adminhtml\Order\Edit\Items;

use Shirtplatform\Core\Model\Config\Source\ProductType;

class Grid {
    const ITEM_IMAGE_TEMPLATE = 'Shirtplatform_Collection::order/create/items/image.phtml';

    /**
     * @var \Shirtplatform\Core\Helper\Image
     */
    private $_imageHelper;

    /**
     * @var \Magento\Framework\View\LayoutInterface
     */
    private $_layout;
    
    /**
     * 
     * @param \Shirtplatform\Core\Helper\Image $imageHelper
     * @param \Magento\Framework\View\LayoutInterface $layout
     */
    public function __construct(\Shirtplatform\Core\Helper\Image $imageHelper,
                                \Magento\Framework\View\LayoutInterface $layout) {
        $this->_imageHelper = $imageHelper;
        $this->_layout = $layout;        
    }
    
    /**
     * Get quote item image HTML. For shirtplatform collection product it renders
     * different template or call the original method if there are no designed
     * images
     * 
     * @access public
     * @param \BrainIndustries\OrderEditor\Block\Adminhtml\Order\Edit\Items\Grid $subject
     * @param \Closure $proceed
     * @param \Magento\Quote\Model\Quote\Item $quoteItem
     * @return string
     */
    public function aroundGetItemImageHtml($subject,
                                           $proceed,
                                           $quoteItem) {
        if ($quoteItem->getShirtplatformProductType() == ProductType::COLLECTION_PRODUCT) {
            $designImages = $this->_imageHelper->getCollectionImages($quoteItem, ['width' => 135, 'height' => 135]);
            $mouseOverImages = $this->_imageHelper->getCollectionImages($quoteItem, ['width' => 200, 'height' => 220]);

            if (!empty($designImages)) {
                $block = $this->_layout
                        ->createBlock(\Magento\Framework\View\Element\Template::class, 'quote_item_image_' . $quoteItem->getId())
                        ->setTemplate(self::ITEM_IMAGE_TEMPLATE)
                        ->setDesignImages($designImages)
                        ->setMouseOverImages($mouseOverImages)
                        ->setAlt($quoteItem->getName());

                return $block->toHtml();
            }
        }

        return $proceed($quoteItem);
    }
}