<?php

namespace Shirtplatform\Collection\Observer\Quote;

use Magento\Framework\Event\ObserverInterface;
use Shirtplatform\Core\Model\Config\Source\ProductType;


class ProductAddAfter implements ObserverInterface
{
    
    /**
     * Set shirtplatform_product_type for quote item in case of collection product
     * 
     * @access public
     * @param \Magento\Framework\Event\Observer $observer          
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quoteItems = $observer->getItems();

        foreach ($quoteItems as $item) {
            $product = $item->getProduct();

            if (empty($item->getShirtplatformProductType()) && $product->getShirtplatformProductType() == ProductType::COLLECTION_PRODUCT) {
                $item->setShirtplatformProductType($product->getShirtplatformProductType());
            }
        }
    }    
}
