<?php

namespace Shirtplatform\Collection\Block\Cart\Item\Renderer;

use Magento\ConfigurableProduct\Block\Cart\Item\Renderer\Configurable;
use Magento\Framework\DataObject\IdentityInterface;
use shirtplatform\resource\PublicResource;

class CollectionProduct extends Configurable implements IdentityInterface {
    /**
     *
     * @var \Shirtplatform\Core\Helper\Image
     */
    private $_imageHelper;

    /**
     * 
     * @param \Shirtplatform\Core\Helper\Image $imageHelper
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Catalog\Helper\Product\Configuration $productConfig
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder
     * @param \Magento\Framework\Url\Helper\Data $urlHelper
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param \Magento\Framework\View\Element\Message\InterpretationStrategyInterface $messageInterpretationStrategy
     * @param array $data
     */
    public function __construct(\Shirtplatform\Core\Helper\Image $imageHelper,
                                \Magento\Framework\View\Element\Template\Context $context,
                                \Magento\Catalog\Helper\Product\Configuration $productConfig,
                                \Magento\Checkout\Model\Session $checkoutSession,
                                \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder,
                                \Magento\Framework\Url\Helper\Data $urlHelper,
                                \Magento\Framework\Message\ManagerInterface $messageManager,
                                \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
                                \Magento\Framework\Module\Manager $moduleManager,
                                \Magento\Framework\View\Element\Message\InterpretationStrategyInterface $messageInterpretationStrategy,
                                array $data = array()) {
        parent::__construct($context, $productConfig, $checkoutSession, $imageBuilder, $urlHelper, $messageManager, $priceCurrency, $moduleManager, $messageInterpretationStrategy, $data);
        $this->_imageHelper = $imageHelper;
    }

    /**
     * Get images with designs
     * 
     * @access public
     * @param \Magento\Quote\Model\Quote\Item $item
     * @param array $dimensions (width, height)
     * @return array
     */
    public function getCollectionImages($item,
                                    $dimensions = []) {                
        if (!isset($dimensions['width'])) {
            $dimensions['width'] = 70;
        }
        if (!isset($dimensions['height'])) {
            $dimensions['height'] = 70;
        }

        return $this->_imageHelper->getCollectionImages($item, $dimensions);
    }
    
    /**
     * Get list of all options for product with additional is_editable_option key
     *      
     * @access public
     * @return array
     */
    public function getOptionList() {
        $result = parent::getOptionList();
        $product = $this->getItem()->getProduct();
        
        foreach ($result as $key => $option) {
            $optionModel = $product->getOptionById($option['option_id']);
            $result[$key]['is_custom_option'] = false;
            
            if ($optionModel) {
                $result[$key]['is_custom_option'] = true;
            }
        }
        
        return $result;
    }
}