<?php

namespace Shirtplatform\Collection\Block\Product\Gallery;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Catalog\Model\Product;
use Magento\Framework\Registry;
use Shirtplatform\Core\Api\AssignedViewRepositoryInterface;
use shirtplatform\resource\PublicResource;

class Reload extends \Magento\Framework\View\Element\Template {

    /**
     * @var AssignedViewRepositoryInterface
     */
    protected $_assignedViewRepository;

    /**
     * @var \Shirtplatform\Core\Helper\Data
     */
    protected $_dataHelper;

    /**
     * @var Registry
     */
    protected $_registry;

    /**
     * @var Product
     */
    protected $_product = null;

    /** @var ScopeConfigInterface */
    private $scopeConfig;

    /**
     * Reload constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Shirtplatform\Core\Helper\Data $dataHelper
     * @param Registry $registry
     * @param AssignedViewRepositoryInterface $assignedViewRepository
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Shirtplatform\Core\Helper\Data $dataHelper,
        Registry $registry,
        AssignedViewRepositoryInterface $assignedViewRepository,
        ScopeConfigInterface $scopeConfig
    ) {
        parent::__construct($context);
        $this->_assignedViewRepository = $assignedViewRepository;
        $this->_dataHelper = $dataHelper;                
        $this->_registry = $registry;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return null|Product
     * @throws LocalizedException
     */
    public function getProduct()
    {
        if (is_null($this->_product)) {
            $this->_product = $this->_registry->registry('product');
            if (!$this->_product->getId()) {
                throw new LocalizedException(__('Failed to initialize product'));
            }
        }
        return $this->_product;
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    public function getImageUrls()
    {
        $imageUrls = $imageIds = [];
        $product = $this->getProduct();
        $images = $product->getMediaGalleryImages();
        if (count($images)) {
            foreach ($images as $_image) {
                $imageIds[] = $_image->getValueId();
            }

            $this->_dataHelper->setWebservicesUrl();
            $assignedViews = $this->_assignedViewRepository->getByGallery($imageIds);
            $templateProductId = $product->getShirtplatformId();
            foreach ($assignedViews as $view) {
                $imageUrls[] = PublicResource::getCollectionProductImageUrl($templateProductId, $view['assigned_view_id']) . '/elements/';
            }
        }
        return json_encode($imageUrls);
    }

    /**
     * @return int
     */
    public function getImageWidth()
    {
        return (int) $this->scopeConfig->getValue('shirtplatform/images/width');
    }

    /**
     * @return int
     */
    public function getImageHeight()
    {
        return (int) $this->scopeConfig->getValue('shirtplatform/images/height');
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    public function getEditableParams()
    {
        $params = [];
        $options = $this->getProduct()->getOptions();
        foreach ($options as $option) {
            $params[] = $option->getSku() . ';text={{options_' . $option->getOptionId() . '_text}}';
        }
        return implode('/', $params);
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    public function getConfigurableColors()
    {
        $assigendColorIds = [];
        $product = $this->getProduct();
        $childProducts = $product->getTypeInstance()->getUsedProducts($product);
        foreach ($childProducts as $childProduct) {
            $assigendColorIds[$childProduct->getId()] = $childProduct->getShirtplatformColorId();
        }
        return json_encode($assigendColorIds);
    }

    /**
     * @return bool
     * @throws LocalizedException
     */
    public function isCollectionProduct()
    {
        return ((int) $this->getProduct()->getShirtplatformProductType() === 1);
    }

}
