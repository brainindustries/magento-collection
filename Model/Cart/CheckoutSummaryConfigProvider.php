<?php

namespace Shirtplatform\Collection\Model\Cart;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Shirtplatform\Core\Model\Config\Source\ProductType;

class CheckoutSummaryConfigProvider implements ConfigProviderInterface {

    /**
     * @var CheckoutSession
     */
    private $_checkoutSession;

    /**
     * @var \Shirtplatform\Core\Helper\Image
     */
    private $_imageHelper;

    /**
     * 
     * @param CheckoutSession $checkoutSession
     * @param \Shirtplatform\Core\Helper\Image $imageHelper
     */
    public function __construct(CheckoutSession $checkoutSession,
                                \Shirtplatform\Core\Helper\Image $imageHelper) {
        $this->_checkoutSession = $checkoutSession;
        $this->_imageHelper = $imageHelper;
    }

    /**
     * Add design and mouseover images to checkout config for collection products
     * 
     * @access public
     * @return array
     */
    public function getConfig() {        
        $output = [
            'design_images' => [],
            'mouseover_images' => []
         ];

        foreach ($this->_checkoutSession->getQuote()->getAllVisibleItems() as $item) {
//            $product = $item->getProduct();

            if ($item->getShirtplatformProductType() == ProductType::COLLECTION_PRODUCT) {                
                $images = $this->_imageHelper->getCollectionImages($item, ['width' => 70, 'height' => 70]);
                $mouseoverImages = $this->_imageHelper->getCollectionImages($item, ['width' => 150, 'height' => 150]);
                $output['design_images']['item_' . $item->getId()] = $images;
                $output['mouseover_images']['item_' . $item->getId()] = $mouseoverImages;
            }
        }
        
        return $output;        
    }

}
